class CreateCharacters < ActiveRecord::Migration
  def change
    create_table :characters do |t|
      t.string :name
      t.string :upB
      t.string :forwardB
      t.string :neutralB
      t.string :downB

      t.timestamps
    end
  end
end
