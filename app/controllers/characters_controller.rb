class CharactersController < ApplicationController
  def index
  	@characters = Character.all
  end

  def show
  	@character = Character.find params[:id]
  end

  def new
  	@character = Character.new
  end

  def create
  	@character = Character.create(name: params[:character][:name], 
  		upB: params[:character][:upB],
  		forwardB: params[:character][:forwardB],
  		neutralB: params[:character][:neutralB],
  		downB: params[:character][:downB]
  		)
  	redirect_to :action => 'index'
  end

  def destroy
  	@character = Character.find params[:id]
  	@character.destroy
  	redirect_to :action => 'index'
  end

  def edit
  	@character = Character.find params[:id]
  end

  def update
  	@character = Character.find params[:id]
  	if @character.update_attributes(name: params[:character][:name], 
  		upB: params[:character][:upB],
  		forwardB: params[:character][:forwardB],
  		neutralB: params[:character][:neutralB],
  		downB: params[:character][:downB])
  		redirect_to :action => 'show', :id => @character.id
  	end
  end
  
end
